//
//  UserViewModel.swift
//  Assigment
//
//  Created by Kaushant on 12/11/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import Foundation
import CoreBluetooth

class UserViewModel {
    
    let locationViewSeguaIdentify = "locationSegue"
    var isLogin = false
    
    var peripherals = [CBPeripheral]()
    
    
    func login(userName: String, password: String, completionHandler: @escaping ((Bool)-> Void)) -> Void {
        self.isLogin = true
        let user = User(userName: userName, password: password)
        
        Router.shared.userLogin(user: user, success: { (data) in
            let token = String(data: data, encoding: String.Encoding.utf8)
           UserDefaults.standard.set(token, forKey: "token")
            DispatchQueue.main.async {
                
                completionHandler(true)
                self.isLogin = false
            }
            
            
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                
                completionHandler(false)
                self?.isLogin = false
            }
           
        }
    }
}
