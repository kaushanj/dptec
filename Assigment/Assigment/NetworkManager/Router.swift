//
//  Routes.swift
//  Assigment
//
//  Created by Kaushant on 12/11/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import Foundation

public typealias ErrorHandler = (String) -> Void


protocol RouteApi {
    func userLogin(user: User, success: @escaping  (Data)->Void, errorHandler: @escaping  ErrorHandler)
}

class Router: RouteApi {
    
    
    static let shared: RouteApi = Router()
    
    private var task: URLSessionDataTask?
    
    private let baseUrl = "http://aunexsimulator.com:7080"
    
    func userLogin(user: User, success: @escaping (Data) -> Void, errorHandler: @escaping ErrorHandler) {
        let url = "\(self.baseUrl)/api/token"
        self.post(urlString: url, user: user, success: success, errorHandler: errorHandler)
    }
    
    
    private func post(urlString: String, user: User, success: @escaping (Data) -> Void, errorHandler: @escaping ErrorHandler ){
        
        guard let url = URL(string: urlString) else {
            errorHandler(ErrorTypes.UrlError.rawValue)
            return
        }
        
        let encoder = JSONEncoder()
        let data = try! encoder.encode(user)
        
        request(url: url, method: .post, parameters: data, completionHandler: { (data, error) in
            
            if let error = error {
                errorHandler(error.localizedDescription)
                return
            }
            
            guard let data = data else {
                errorHandler(ErrorTypes.responseDataError.rawValue)
                return
            }
            success(data)

    
        })
    }
    
    private func request(url: URL, method: HTTPMethod = .get, parameters: Data? , completionHandler: @escaping ((_ data :Data?  , _ error: Error?) -> Void) ) {
        
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.httpBody = parameters
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    completionHandler(data, nil)
                }
                else{
                    completionHandler(nil, ErrorTypes.resposeError )
                }
            }
            
            completionHandler(data, err)
        }
        task?.resume()
        
    }
    
    func cancelTask() -> Void {
        self.task?.cancel()
    }
    
}


extension Router {
    
    enum ErrorTypes: String, Error {
        case UrlError = "URL Error"
        case resposeError = "SomeThing went worng, please try again later"
        case responseDataError = "Please Contact Administrator"
    }
    
    enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
    }
}
