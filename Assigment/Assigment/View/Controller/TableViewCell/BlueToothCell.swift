//
//  BlueToothCell.swift
//  Assigment
//
//  Created by Thilina Kaushan on 12/11/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit

class BlueToothCell: UITableViewCell {

    @IBOutlet weak var blueToothLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
