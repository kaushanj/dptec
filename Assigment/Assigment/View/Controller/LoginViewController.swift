//
//  ViewController.swift
//  Assigment
//
//  Created by Kaushant on 12/11/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    var viewModel : UserViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.viewModel = UserViewModel()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.viewModel.locationViewSeguaIdentify {
            if let destination = segue.destination as? LocationViewController {
                
                destination.viewModel = self.viewModel
            }
        }
    }
    @IBAction func loginBtnTao(_ sender: Any) {
        
        let userName = userNameTxt.text!
        let password = passwordTxt.text!
        guard  !userName.isEmpty && !password.isEmpty else {
            return
        }
        
        
        
        let myContext = LAContext()
        let myLocalizedReasonString = "Biometric Authntication testing !! "
        var authError: NSError?
        
        if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { [weak self](success, error) in

                if success  {
                    
                    self?.viewModel.login(userName: userName, password: password, completionHandler: { (success) in
                        
                        if success {
                            if !self!.viewModel.isLogin { return}
                            
                            self?.performSegue(withIdentifier: (self?.viewModel.locationViewSeguaIdentify)!, sender: nil)
                        }
                    })
                }
                else {

                }
            }
        }
        
        
    }
    
}

extension LoginViewController {
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

