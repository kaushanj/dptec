//
//  LocationViewController.swift
//  Assigment
//
//  Created by Kaushant on 12/11/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit
import CoreLocation
import CoreBluetooth

class LocationViewController: UIViewController {
    
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    
    var viewModel: UserViewModel!
    
    @IBOutlet weak var latitudeLbl: UILabel!
    @IBOutlet weak var longitudeLbt: UILabel!
    @IBOutlet weak var blueToothTableView: UITableView!
    
    var centralManager: CBCentralManager?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        startLocation = nil
        
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LocationViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let latestLocation: CLLocation = locations[locations.count - 1]
        print(latestLocation.coordinate.latitude)
        let latitude = String(format: "%.4f",
                               latestLocation.coordinate.latitude)
        let longitude = String(format: "%.4f",
                                latestLocation.coordinate.longitude)
        
        self.latitudeLbl.text = latitude
        self.longitudeLbt.text = longitude
        
        self.locationManager.stopUpdatingLocation()
    }
}

extension LocationViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if (central.state == .poweredOn){
            self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
        }
        else {
            
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if !self.viewModel.peripherals.contains(peripheral){
            guard peripheral.name != nil else {
                return
            }
            self.viewModel.peripherals.append(peripheral)
            self.blueToothTableView.reloadData()
        }
        
    }
}

extension LocationViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.peripherals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BlueToothCell.self), for: indexPath) as! BlueToothCell
        let blueTooth = self.viewModel.peripherals[indexPath.row]
        cell.blueToothLbl.text = blueTooth.name
        
        return cell
    }
    
    
}
