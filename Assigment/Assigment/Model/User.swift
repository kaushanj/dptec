//
//  User.swift
//  Assigment
//
//  Created by Kaushant on 12/11/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import Foundation


struct User: Codable {
    let UserName: String
    let Password: String
    
    init(userName: String, password: String) {
        self.UserName = userName
        self.Password = password
    }
}
